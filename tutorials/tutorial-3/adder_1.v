module adder_1( input a, b, output c );

	assign c = a + b;

endmodule
