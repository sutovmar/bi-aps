module test();
	reg [31:0] a; // vstupne argumenty su reg
	reg clk, reset;
	wire [31:0] result;
	//wire[31:0] y; // vystupne argumenty su ako draty

	resetable_register obvodik(a,clk, reset, result);

	initial begin
		$dumpfile("test");
		$dumpvars;
		a = 32'b1;
		reset = 0;
		#160 $finish;
	end

	always #3 a = a +1;
	always #5 reset = ~reset;
	
	always begin
		clk <= 0; #1; clk <= 1; #1;
	end

endmodule
