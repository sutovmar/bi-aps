module multiplexer_2_1(input [31:0] d0, d1, input select, output [31:0] y );
	
	assign y = select?d0:d1;

endmodule