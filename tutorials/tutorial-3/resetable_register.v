module resetable_register(input [31:0] a0, input clk, reset, output [31:0] rd1);

	reg [31:0] rd1_tmp;

	assign rd1 = rd1_tmp;

	always@(posedge clk) begin
		if( reset )
			rd1_tmp = 0;
		else
			rd1_tmp = a0;
	end


endmodule
