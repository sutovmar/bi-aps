//behavioral ?
module second_task ( input a, b, c, d, output y);
	
	reg y_output;
	
	assign y = y_output;

	always@(*) begin

		if( a == 1)
			y_output = b & c;
		else
			y_output = (b ^ d) | c;
	end

endmodule
