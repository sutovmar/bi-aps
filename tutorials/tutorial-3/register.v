module register(input [31:0] datain, input clk, output [31:0] dataout);
	
	reg [31:0] dataout_tmp;
	
	assign dataout = dataout_tmp;
	
	always@(posedge clk) begin
		dataout_tmp = datain;
	end

endmodule
