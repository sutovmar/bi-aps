.data                  # directive indicating start of the data segment
.align  2              # set data alignment to 4 bytes

array:                 # label - name of the memory block
.word  3, 7, 3, 5, 1   # values in the array to increment...

.text                  # beginning of the text segment (or code segment)

# TODO: Write your code here

beq $0, $0, start

set_new_max:
	addi $t0, $t1,0
	beq $0, $0, loop

find_max:
	addi $t0, $zero, 0	

loop:
	add $t2, $a0, $a1  
	lw $t1, 0($t2)
	
	slt $t5, $t0, $t1
	beq $t5, 1, set_new_max
	
	
	addi $a1, $a1, 1
	
	bne $a1, 5, loop
	jr $ra
	

start:
la   $a0, array         # store address of the "array" to the register s0
addi $a1, $zero, 0

jal find_max