module test();
	
	reg [31:0] in;
	wire [31:0] out;

	
	multiply_4 test(in,out);

	initial begin
		$dumpfile("multiply");
		$dumpvars;
		in = 32'b1100;
		#200 $finish;
	end

	always #10 in = in + 1;

endmodule
