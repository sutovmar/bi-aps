module adder_32( input [31:0] a,b, input c0, output [31:0] sum, output s32 );

	assign {sum, s32} = a + b + c0;

endmodule
