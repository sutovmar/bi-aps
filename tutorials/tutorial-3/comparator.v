module comparator( input [31:0] a1, a0, output result);
	
	assign result = ( a1 == a0 ) ? 1 : 0;

endmodule
