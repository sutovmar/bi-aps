module register(input [4:0] a1, a2, a3, input [31:0] wd3, input clk, we, output [31:0] rd1, rd2);

	//registers
	reg [31:0] rf[31:0];	

	initial begin
		rf[0] = 32'b0;
	end
	
	reg [31:0] readoutput_1, readoutput_2;

	assign rd1 = readoutput_1;
	assign rd2 = readoutput_2;

	always@(a1,a2) begin
		readoutput_1 = rf[a1];
		readoutput_2 = rf[a2];
	end

	always@(posedge clk) begin
		if (we == 1 && a3 != 0 )
			rf[a3] = wd3;
	end

endmodule
