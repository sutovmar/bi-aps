module test();

	reg[4:0] a1,a2,a3;
	reg[31:0] wd3;
	reg clk, we;

	wire[31:0] rd1, rd2;
	
	register regs(a1,a2,a3, wd3, clk, we, rd1, rd2);

	initial begin
		$dumpfile("reg");
		$dumpvars;
		we = 1'b1;
		wd3 = 32'b100;
		a3 = 5'b100;
		//---------
		#50 we = 1'b0;
		#50 a1 = 5'b100;
		#50 a2 = 5'b100;
		//----------
		#100 we = 1'b1;
		#100 wd3 = 32'b010;
		//-----------
		#150 we = 1'b0;
		#150 a1 = 5'b100;
		#150 a2 = 5'b100;

		#200 $finish;
	end
 

	always begin
		clk <= 0; #1; clk <= 1; #1;
	end

endmodule
