//ALU Operation Decoder

module ALUOpDecoder(input [4:0] shamt, input [5:0] funct, input [1:0] ALUOp, output [3:0] ALUControl );

	reg[3:0] ALUControl_tmp;

	assign ALUControl = ALUControl_tmp;

	always @* begin

		if ( ALUOp == 2'b00 && shamt == 5'bX && funct == 6'bX )
			ALUControl_tmp = 4'b0010;
		
		if ( ALUOp == 2'b01 && shamt == 5'bX && funct == 6'bX )
			ALUControl_tmp = 4'b0110;

		if ( ALUOp == 2'b10 && funct == 6'b100000 && shamt == 5'bX )
			ALUControl_tmp = 4'b0010;
		
		if ( ALUOp == 2'b10 && funct == 6'b100010 && shamt == 5'bX)
			ALUControl_tmp = 4'b0110;

		if ( ALUOp == 2'b10 && funct == 6'b100100 && shamt == 5'bX)
			ALUControl_tmp = 4'b0000;

		if ( ALUOp == 2'b10 && funct == 6'b100101 && shamt == 5'bX)
			ALUControl_tmp = 4'b0001;

		if ( ALUOp == 2'b10 && funct == 6'b101010 && shamt == 5'bX)
			ALUControl_tmp = 4'b0111;

		if ( ALUOp == 2'b11 && funct == 6'b010000 && shamt == 5'b0)
			ALUControl_tmp = 4'b1000;

		if ( ALUOp == 2'b11 && funct == 6'b010000 && shamt == 5'b00100)
			ALUControl_tmp = 4'b1001;

	end


endmodule
