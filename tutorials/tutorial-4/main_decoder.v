//Main Decoder of Control Unit

module MainDecoder( input [5:0] opcode, output [1:0] ALUOp, output PCSrcJal, PCSrcJr, RegWrite, MemToReg, MemWrite, RegDst, Branch, ALUSrc );

		reg t_RegWrite ;
		reg t_RegDst   ;
		reg t_ALUSrc	;
		reg[1:0] t_ALUOp;
		reg t_Branch	;
		reg t_MemWrite ;
		reg t_MemToReg	;
		reg t_PCSrcJr	;
		reg t_PCSrcJal	; 

		assign RegWrite 	= t_RegWrite;
		assign RegDst   	= t_RegDst;
		assign ALUSrc		= t_ALUSrc;
		assign ALUOp		= t_ALUOp;
		assign Branch		= t_Branch;
		assign MemWrite 	= t_MemWrite;
		assign MemToReg		= t_MemToReg;
		assign PCSrcJr		= t_PCSrcJr;
		assign PCSrcJal		= t_PCSrcJal;

		always @* begin

		if( opcode == 6'b000000 )
			t_RegWrite 	= 1;
			t_RegDst   	= 1;
			t_ALUSrc	= 0;
			t_ALUOp		= 10;
			t_Branch	= 0;
			t_MemWrite 	= 0;
			t_MemToReg	= 0;
			t_PCSrcJr	= 0;
			t_PCSrcJal	= 0;

		// LW 
		if( opcode == 6'b100011 )
			t_RegWrite	= 1;
			t_RegDst	= 1;
			t_ALUSrc	= 0;
			t_ALUOp		= 10;
			t_Branch	= 0;
			t_MemWrite 	= 0;
			t_MemToReg	= 1;
			t_PCSrcJal	= 0;
			t_PCSrcJr	= 0;

		//SW
		if( opcode == 6'b101011 )
			t_RegWrite	= 0;
			t_RegDst	= 'bx;
			t_ALUSrc 	= 1;
			t_ALUOp		= 00;
			t_Branch	= 0;
			t_MemWrite 	= 1;
			t_MemToReg 	= 'bx;
			t_PCSrcJal	= 0;
			t_PCSrcJr	= 0;

		//BEQ
		if( opcode == 6'b000100 )
			t_RegWrite	= 0;
			t_RegDst	= 'bx;
			t_ALUSrc	= 0;
			t_ALUOp		= 01;
			t_Branch 	= 1;
			t_MemWrite	= 0;
			t_MemToReg	= 'bx;
			t_PCSrcJr	= 0;
			t_PCSrcJal	= 0;

		//ADDI
		if( opcode == 6'b001000 )
			t_RegWrite	= 1;
			t_RegDst	= 0;
			t_ALUSrc	= 1;
			t_ALUOp		= 00;
			t_Branch 	= 0;
			t_MemWrite 	= 0;
			t_MemToReg	= 0;
			t_PCSrcJal	= 0;
			t_PCSrcJr	= 0;

		//JAL
		if( opcode == 6'b000011 )
			t_RegWrite	= 1;
			t_RegDst	= 'bx;
			t_ALUSrc	= 'bx;
			t_ALUOp		= 2'bxx;
			t_Branch 	= 'bx;
			t_MemWrite	= 0;
			t_MemToReg	= 'bx;
			t_PCSrcJal	= 1;
			t_PCSrcJr	= 0;

		//JR
		if( opcode == 6'b000111 )
			t_RegWrite	= 0;
			t_RegDst	= 'bx;
			t_ALUSrc	= 'bx;
			t_ALUOp		= 2'bxx;
			t_Branch 	= 'bx;
			t_MemWrite	= 0;
			t_MemToReg	= 'bx;
			t_PCSrcJal	= 0;
			t_PCSrcJr	= 1;

		//ADDU
		if( opcode == 6'b011111 )
			t_RegWrite	= 1;
			t_RegDst	= 1;
			t_ALUSrc	= 0;
			t_ALUOp		= 11;
			t_Branch	= 0;
			t_MemWrite	= 0;
			t_MemToReg	= 0;
			t_PCSrcJal	= 0;
			t_PCSrcJr	= 0;

		end

endmodule
