module extended_adder(input[31:0] a,b, output[31:0] result);

	genvar i;

	for( i = 0; i < 32; i = i +1 ) begin
		adder_1 someshit(a[i], b[i], result[i]);
	end

endmodule
