module test();

	reg [31:0] a,b;
	reg [3:0] control;
	wire [31:0] result;
	wire zero;
	
	alu ALUControlUnit(a,b, control, result, zero);
	
	initial begin
		$dumpfile("alutest");
		$dumpvars;
		a = 32'b1;
		b = 32'b001;
		control = 4'b0000;
		#200 $finish;
	end
	
	always #10 control=control+1;

endmodule
