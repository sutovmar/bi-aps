module test();
  reg [31:0] a,b;
  reg [3:0] c_log_arith;
  wire [31:0] op;
  wire zero;

   alu alu_test(a,b, c_log_arith, op, zero);

   initial
     begin
    $dumpfile("alu");
    $dumpvars;
    a=32'b0011;
    b=32'b1101;
    c_log_arith=4'b00;
    #100;
   
    a=32'b1101;
    b=32'b1111;
    c_log_arith=4'b01;
    #100;
   
    a=32'b0011;
    b=32'b1100;
    c_log_arith=4'b10;
    #100;
   
    a=32'b1110;
    b=32'b0100;
    c_log_arith=4'b11;
    #100;

    a=32'b0111;
    b=32'b1001;
    c_log_arith=4'b00;
    #100;
   
    a=32'b1111;
    b=32'b1111;
    c_log_arith=4'b01;
    #100;
   
    a=32'b1011;
    b=32'b1101;
    c_log_arith=4'b10;
    #100;
   
    a=32'b1110;
    b=32'b0111;
    c_log_arith=4'b11;
    #100
    $stop;
     end
   endmodule
