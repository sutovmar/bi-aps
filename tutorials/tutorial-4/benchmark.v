module test();
	reg [31:0] a,b; // vstupne argumenty su reg
	//reg clk, reset;
	wire [31:0] result;
	//wire[31:0] y; // vystupne argumenty su ako draty

	extended_adder obvodik(a,b,result);

	initial begin
		$dumpfile("test");
		$dumpvars;
		a = 32'b1;
		b = 32'b10;
		#160 $finish;
	end

	always #80 a = a +1;
	always #40 b = b + 2;
	
	//always begin
	//	clk <= 0; #1; clk <= 1; #1;
	//end

endmodule
