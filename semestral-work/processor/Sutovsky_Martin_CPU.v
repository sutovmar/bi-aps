module processor( 
                 input         clk, reset,
                  output [31:0] PC,
                  input  [31:0] instruction,
                  output        WE,
                  output [31:0] address_to_mem,
                  output [31:0] data_to_mem,
                  input  [31:0] data_from_mem

                );

  reg 	[31:0] PC_tmp;
  wire 	PCSrcJal, PCSrcJr, RegWrite, MemToReg, ALUSrc, RegDst, Branch, zero, carry, PCSrcBeq;
  wire 	[3:0] alu_control;
  wire  [4:0] a3_reg, a3;
  wire 	[31:0] rd1, rd2, wd3, srcB, address_to_mem,SignImm, result, PC_tmp_tmp, PC_incr, PCBranch, PCJal;

  sign_extension processor_sign_extension(instruction[15:0], SignImm);

  ControlUnit proc_control_unit( instruction[31:26], instruction[5:0], instruction[10:6] , alu_control, PCSrcJal, PCSrcJr, RegWrite, MemToReg, WE, ALUSrc, RegDst, Branch);
  
  multiplexer_2_1 a3_input(instruction[15:11], instruction[20:16], RegDst, a3);
  
  multiplexer_2_1_32bit result_multiplexer(data_from_mem, address_to_mem, MemToReg, result);
  
  adder_32 branch_adder( (SignImm << 2) , PC_incr, 1'b0, PCBranch );
                                
  alu processor_alu(rd1, srcB, alu_control, address_to_mem, zero);
  
  adder_32 pc_adder(PC_tmp, 4, 1'b0, PC_incr);
  
  register processor_register( instruction[25:21], instruction[20:16], a3_reg, wd3 , clk,  RegWrite, rd1, data_to_mem);
  
  multiplexer_2_1_32bit wd3_multiplexer( PC_incr, result, PCSrcJal, wd3);

  multiplexer_2_1_32bit a2_alu(SignImm, data_to_mem, ALUSrc, srcB);

  assign PCJal = {PC_incr[31:28], instruction[25:0], 2'b00};

  multiplexer_2_1 jal_multiplexer(5'd31, a3, PCSrcJal, a3_reg);

  always@( posedge clk )
  begin
		if( reset == 1 )
          	PC_tmp = 32'b0;
      	else
      		PC_tmp = PC_tmp_tmp;
  end

  assign PC = PC_tmp;

  multiplexer_4_1_32bit PC_multiplexer(rd1, PCJal, PCBranch, PC_incr, PCSrcJr, PCSrcJal, zero && Branch, PC_tmp_tmp);

endmodule


//===============================================================================================

module alu( input [31:0] srcA, input [31:0] srcB, input [3:0] ALUControl,output [31:0] ALUResult, output zero);
	
	reg [31:0] ALUResult_tmp;
	reg zero_tmp = 0;

	integer i;

	assign ALUResult = ALUResult_tmp;
	assign zero = zero_tmp;
   
	always@(*) begin
			case(ALUControl)
				4'b0010: ALUResult_tmp = srcA + srcB;
				4'b0110: ALUResult_tmp = srcA - srcB;
				4'b0000: ALUResult_tmp = srcA & srcB;
				4'b0001: ALUResult_tmp = srcA | srcB;
				4'b0011: ALUResult_tmp = srcA ^ srcB;

				4'b0111: ALUResult_tmp = $signed(srcA) < $signed(srcB);

				4'b1000: 
				begin
					// $display("ADDU %d %d\n", srcA, srcB);
					ALUResult_tmp[7:0] = srcA[7:0] + srcB[7:0];
					ALUResult_tmp[15:8] = srcA[15:8] + srcB[15:8];
					ALUResult_tmp[23:16] = srcA[23:16] + srcB[23:16];
					ALUResult_tmp[31:24] = srcA[31:24] + srcB[31:24]; 
				end
				4'b1001:
				begin 
					// $display("Saturated ADDU %d %d\n", srcA, srcB);
					ALUResult_tmp[7:0] 		= 	( srcA[7:0] + srcB[7:0] ) < srcA[7:0] ? 255 : ( srcA[7:0] + srcB[7:0] );
					ALUResult_tmp[15:8] 	= 	( srcA[15:8] + srcB[15:8] ) < srcA[15:8] ? 255 : ( srcA[15:8] + srcB[15:8] );
					ALUResult_tmp[23:16] 	= 	( srcA[23:16] + srcB[23:16] ) < srcA[23:16] ? 255 : ( srcA[23:16] + srcB[23:16] );
					ALUResult_tmp[31:24] 	= 	( srcA[31:24] + srcB[31:24] ) < srcA[31:24] ? 255 : ( srcA[31:24] + srcB[31:24] ); 
				end
		endcase

		zero_tmp <= (( ALUResult_tmp == 0) ? ( 1 ) : 0);
	end

endmodule

//===============================================================================================

module adder_32( input [31:0] a,b, input c0, output [31:0] sum );
	assign sum = a+b;
endmodule

//===============================================================================================

module ALUOpDecoder(input [4:0] shamt, input [5:0] funct, input [1:0] ALUOp, output [3:0] ALUControl );

	reg[3:0] ALUControl_tmp;

	wire [12:0] ALUIn;

	assign ALUControl = ALUControl_tmp;

	 always @(*)
    	case ( ALUOp )
	        'b00:               ALUControl_tmp = 'b0010; // addition
	        'b01:               ALUControl_tmp = 'b0110; // subtraction
	        'b10: begin
	            case ( funct )
	                'b100000:   ALUControl_tmp = 'b0010; // addition
	                'b100010:   ALUControl_tmp = 'b0110; // substraction
	                'b100100:   ALUControl_tmp = 'b0000; // and
	                'b100101:   ALUControl_tmp = 'b0001; // or
	                'b101010:   ALUControl_tmp = 'b0111; // set less than
	            endcase
	            end
	        'b11:
	            case ( funct + shamt )
	                'b010000:   ALUControl_tmp = 'b1000; // addition byte after byte
	                'b010100:   ALUControl_tmp = 'b1001; // saturted addition byte after byte
	            endcase
    endcase

endmodule


//===============================================================================================

module ControlUnit( 
	input [5:0] opcode, funct, input [4:0] shamt, 
	output [3:0] ALUControl,
	output PCSrcJal, PCSrcJr, RegWrite, MemToReg, MemWrite, ALUSrc, RegDst, Branch );
	wire [1:0] ALUOp;

	MainDecoder MainDecoder( opcode, ALUOp, PCSrcJal, PCSrcJr, RegWrite, MemToReg, MemWrite, RegDst, Branch, ALUSrc );

	ALUOpDecoder ALUOpDecoder( shamt, funct, ALUOp, ALUControl);

endmodule


//===============================================================================================

module MainDecoder(  input [5:0] opcode, output [1:0] ALUOp, output PCSrcJal, PCSrcJr, RegWrite, MemToReg, MemWrite, RegDst, Branch, ALUSrc );

		reg t_RegWrite 		= 'b0;
		reg t_RegDst 		= 'b0;
		reg t_ALUSrc		= 'b0;
		reg[1:0] t_ALUOp 	= 2'b0;
		reg t_Branch		= 'b0;
		reg t_MemWrite 		= 'b0;
		reg t_MemToReg		= 'b0;
		reg t_PCSrcJr		= 'b0;
		reg t_PCSrcJal		= 'b0;

		assign RegWrite 	= t_RegWrite;
		assign RegDst   	= t_RegDst;
		assign ALUSrc		= t_ALUSrc;
		assign ALUOp		= t_ALUOp;
		assign Branch		= t_Branch;
		assign MemWrite 	= t_MemWrite;
		assign MemToReg		= t_MemToReg;
		assign PCSrcJr		= t_PCSrcJr;
		assign PCSrcJal		= t_PCSrcJal;

		always @(*) begin

			case(opcode)
			
				6'b000000: {t_RegWrite, t_RegDst, t_ALUSrc, t_ALUOp, t_Branch, t_MemWrite, t_MemToReg, t_PCSrcJal, t_PCSrcJr } = 10'b1101000000;
							
				//LW
				6'b100011: {t_RegWrite, t_RegDst, t_ALUSrc, t_ALUOp, t_Branch, t_MemWrite, t_MemToReg, t_PCSrcJal, t_PCSrcJr } = 10'b1010000100;
				
				//SW
				6'b101011: {t_RegWrite, t_RegDst, t_ALUSrc, t_ALUOp, t_Branch, t_MemWrite, t_MemToReg, t_PCSrcJal, t_PCSrcJr } = 10'b0010001000;

				//BEQ
				6'b000100: {t_RegWrite, t_RegDst, t_ALUSrc, t_ALUOp, t_Branch, t_MemWrite, t_MemToReg, t_PCSrcJal, t_PCSrcJr } = 10'b0000110000;
				
				//ADDI
				6'b001000: {t_RegWrite, t_RegDst, t_ALUSrc, t_ALUOp, t_Branch, t_MemWrite, t_MemToReg, t_PCSrcJal, t_PCSrcJr } = 10'b1010000000;
					
				//JAL
				6'b000011: {t_RegWrite, t_RegDst, t_ALUSrc, t_ALUOp, t_Branch, t_MemWrite, t_MemToReg, t_PCSrcJal, t_PCSrcJr } = 10'b1000000010;
				
				//JR
				6'b000111: {t_RegWrite, t_RegDst, t_ALUSrc, t_ALUOp, t_Branch, t_MemWrite, t_MemToReg, t_PCSrcJal, t_PCSrcJr } = 10'b0000000001;
				
				//ADDU
				6'b011111: {t_RegWrite, t_RegDst, t_ALUSrc, t_ALUOp, t_Branch, t_MemWrite, t_MemToReg, t_PCSrcJal, t_PCSrcJr } = 10'b1101100000;		
				
			endcase
		end
endmodule

//===============================================================================================

module multiplexer_2_1(input [4:0] d0, d1, input select, output [4:0] y );
	
	assign y = select?d0:d1;

endmodule

//===============================================================================================

module multiplexer_2_1_32bit(input [31:0] d0, d1, input select, output [31:0] y );
	
	assign y = select?d0:d1;

endmodule

//===============================================================================================

module multiplexer_4_1_32bit ( input [31:0] d1, d2, d3, d4,
                input select1, select2, select3,
                output reg [31:0] out );
	always @(*)
		begin
			if ( select1 )
				out = d1;
			else if ( select2 )
				out = d2;
			else if ( select3 )
				out = d3;
			else
				out = d4;
		end
endmodule

//===============================================================================================

module register(input [4:0] a, b, c, input [31:0] wd3, input clk, we, output [31:0] rd1, rd2);

	reg [31:0] rf[31:0];	
	integer i;
	reg [31:0] readoutput_1, readoutput_2;

	initial begin
		for( i =0 ; i < 32; i++ )
			begin
				rf[i] = 32'b0;
			end
	end

	assign rd1 = readoutput_1;
	assign rd2 = readoutput_2;

	always@(a,b, rf[a], rf[b]) begin
		readoutput_1 = rf[a];
		readoutput_2 = rf[b];
	end

	always@(posedge clk) begin
		if ( we == 1 && c != 5'b0 )
			rf[c] = wd3;
	end

endmodule

//===============================================================================================

module sign_extension(input [15:0] a, output [31:0] extended);
	assign extended = { { 16{a[15]}} , a };
endmodule
