beq $0, $0, start

end_sort:
	jr $ra

swap_elements:
	beq $s0, $s1, increment
	sw $s0, 0($t1)
	sw $s1, 0($a2)
	addi $a2, $a0, 0
	addi $a3, $0, 0
	
	beq $0, $0, sort
main:
	sub $a1, $a1, 1
	addi $a2, $a0, 0 # array iterator
	addi $a3, $0, 0
sort:
	beq $a3, $a1, end_sort # end of iteration
	
	#load elements
	
	addi $t1, $a2 ,4  # i + 1
		
	# -- this part can be substitued with addi ---

	lw $s0, 0($a2) # load i-th elemennt
	lw $s1, 0($t1) # load (i+1)-th element
	slt $s2, $s0, $s1
	beq $s2, 0, swap_elements	
	
increment:	
	addi $a2, $a2, 4
	addi $a3, $a3, 1
	beq $0, $0, sort

start:
	lw $a1, 0x8($0) # size of array
	lw $a0, 0xc($0) # actual address of array
	jal main
