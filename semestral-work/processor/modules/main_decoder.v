//Main Decoder of Control Unit

module MainDecoder(  input [5:0] opcode, output [1:0] ALUOp, output PCSrcJal, PCSrcJr, RegWrite, MemToReg, MemWrite, RegDst, Branch, ALUSrc );

		reg t_RegWrite 		= 'b0;
		reg t_RegDst 		= 'b0;
		reg t_ALUSrc		= 'b0;
		reg[1:0] t_ALUOp 	= 2'b0;
		reg t_Branch		= 'b0;
		reg t_MemWrite 		= 'b0;
		reg t_MemToReg		= 'b0;
		reg t_PCSrcJr		= 'b0;
		reg t_PCSrcJal		= 'b0 ;

		assign RegWrite 	= t_RegWrite;
		assign RegDst   	= t_RegDst;
		assign ALUSrc		= t_ALUSrc;
		assign ALUOp		= t_ALUOp;
		assign Branch		= t_Branch;
		assign MemWrite 	= t_MemWrite;
		assign MemToReg		= t_MemToReg;
		assign PCSrcJr		= t_PCSrcJr;
		assign PCSrcJal		= t_PCSrcJal;

		
		always @(opcode) begin

			case(opcode)
			
				6'b000000: begin

						t_RegWrite 	= 1;
						t_RegDst   	= 1;
						t_ALUSrc	= 0;
						t_ALUOp		= 10;
						t_Branch	= 0;
						t_MemWrite 	= 0;
						t_MemToReg	= 0;
						t_PCSrcJr	= 0;
						t_PCSrcJal	= 0;
				end			
				
				//LW
				6'b100011: begin

						t_RegWrite	= 1;
						t_RegDst	= 1;
						t_ALUSrc	= 0;
						t_ALUOp		= 00;
						t_Branch	= 0;
						t_MemWrite 	= 0;
						t_MemToReg	= 1;
						t_PCSrcJal	= 0;
						t_PCSrcJr	= 0;
				end

				//SW
				6'b101011:
					begin
					t_RegWrite	= 0;
					t_RegDst	= 'bx;
					t_ALUSrc 	= 1;
					t_ALUOp		= 00;
					t_Branch	= 0;
					t_MemWrite 	= 1;
					t_MemToReg 	= 'bx;
					t_PCSrcJal	= 0;
					t_PCSrcJr	= 0;
				end
				
				//BEQ
				6'b000100:
					begin
					t_RegWrite	= 0;
					t_RegDst	= 'bx;
					t_ALUSrc	= 0;
					t_ALUOp		= 01;
					t_Branch 	= 1;
					t_MemWrite	= 0;
					t_MemToReg	= 'bx;
					t_PCSrcJr	= 0;
					t_PCSrcJal	= 0;
				end
				
				//ADDI
				6'b001000:
					begin
					t_RegWrite	= 1;
					t_RegDst	= 0;
					t_ALUSrc	= 1;
					t_ALUOp		= 00;
					t_Branch 	= 0;
					t_MemWrite 	= 0;
					t_MemToReg	= 0;
					t_PCSrcJal	= 0;
					t_PCSrcJr	= 0;
				end
				//JAL
				6'b000011:
					begin
					t_RegWrite	= 1;
					t_RegDst	= 'bx;
					t_ALUSrc	= 'bx;
					t_ALUOp		= 2'bxx;
					t_Branch 	= 'b0;
					t_MemWrite	= 0;
					t_MemToReg	= 'bx;
					t_PCSrcJal	= 1;
					t_PCSrcJr	= 0;
				end
				//JR
				6'b000111:
					begin
					t_RegWrite	= 0;
					t_RegDst	= 'bx;
					t_ALUSrc	= 'bx;
					t_ALUOp		= 2'bxx;
					t_Branch 	= 'b0;
					t_MemWrite	= 0;
					t_MemToReg	= 'bx;
					t_PCSrcJal	= 0;
					t_PCSrcJr	= 1;
				end
				//ADDU
				6'b011111:
					begin
					t_RegWrite	= 1;
					t_RegDst	= 1;
					t_ALUSrc	= 0;
					t_ALUOp		= 11;
					t_Branch	= 0;
					t_MemWrite	= 0;
					t_MemToReg	= 0;
					t_PCSrcJal	= 0;
					t_PCSrcJr	= 0;
				end

			endcase
		end

endmodule
