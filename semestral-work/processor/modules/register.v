module register(input [4:0] a1, a2, a3, input [31:0] wd3, input clk, we, output [31:0] rd1, rd2);

	reg [31:0] rf[32:0];	
	integer i;

	initial begin
		for( i =0 ; i < 32; i++ )
			begin
				rf[i] = 32'b0;
			end
	end
	
	reg [31:0] readoutput_1, readoutput_2;

	assign rd1 = readoutput_1;
	assign rd2 = readoutput_2;

	always@(a1,a2, a3, wd3) begin
		readoutput_1 <= rf[a1];
		readoutput_2 <= rf[a2];
	end

	always@(posedge clk) begin
		if ( we == 1 )
			rf[a3] <= wd3;
	end

endmodule
