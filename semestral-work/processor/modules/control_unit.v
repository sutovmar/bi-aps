//Control Unit Module

module ControlUnit( 
	input [5:0] opcode, funct, input [4:0] shamt, 
	output [3:0] ALUControl,
	output PCSrcJal, PCSrcJr, RegWrite, MemToReg, MemWrite, ALUSrc, RegDst, Branch );
	
	wire [1:0] ALUOp;

	MainDecoder MainDecoder( opcode, ALUOp, PCSrcJal, PCSrcJr, RegWrite, MemToReg, MemWrite, RegDst, Branch, ALUSrc );

	ALUOpDecoder ALUOpDecoder( shamt, funct, ALUOp, ALUControl);

endmodule
