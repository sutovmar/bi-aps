//ALU Operation Decoder

module ALUOpDecoder(input [4:0] shamt, input [5:0] funct, input [1:0] ALUOp, output [3:0] ALUControl );

	reg[3:0] ALUControl_tmp;

	wire [12:0] ALUIn;

	assign ALUIn = {ALUOp, funct, shamt};

	assign ALUControl = ALUControl_tmp;

	always @(ALUIn) begin

		casex (ALUIn)
		
			13'b00??????????? : ALUControl_tmp = 4'b0010;
			
			13'b01??????????? : ALUControl_tmp = 4'b0110;

			13'b10100000?????: ALUControl_tmp = 4'b0010;
		
			13'b10100010????? : ALUControl_tmp = 4'b0110;

			13'b10100100????? : ALUControl_tmp = 4'b0000;

			13'b10100101????? :	ALUControl_tmp = 4'b0001;

			13'b10101010????? : ALUControl_tmp = 4'b0111;

			13'b1101000000000 : ALUControl_tmp = 4'b1000;

			13'b1101000000100 :	ALUControl_tmp = 4'b1001;

			default:			ALUControl_tmp = 4'b0000;

		endcase
	end


endmodule
