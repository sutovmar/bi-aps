module sign_extension(input [15:0] a, output [31:0] extended);
	assign extended = { { 16{a[15]}} , a };
endmodule
