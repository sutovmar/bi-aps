module alu( input [31:0] srcA, input [31:0] srcB, input [3:0] ALUControl,output [31:0] ALUResult, output zero);
	
	reg [31:0] ALUResult_tmp;
	reg zero_tmp = 0;

	integer i;

	assign ALUResult = ALUResult_tmp;
	assign zero = zero_tmp;
   
	always@(*) begin
			case(ALUControl)
				4'b0010: ALUResult_tmp = srcA + srcB;
				4'b0110: ALUResult_tmp = srcA - srcB;
				4'b0000: ALUResult_tmp = srcA & srcB;
				4'b0001: ALUResult_tmp = srcA | srcB;
				4'b0011: ALUResult_tmp = srcA ^ srcB;
				4'b0111: ALUResult_tmp = srcA < srcB;
				4'b100: 
				begin
					ALUResult_tmp[7:0] = srcA[7:0] + srcB[7:0];
					ALUResult_tmp[15:8] = srcA[15:8] + srcB[15:8];
					ALUResult_tmp[23:16] = srcA[23:16] + srcB[23:16];
					ALUResult_tmp[31:24] = srcA[31:24] + srcB[31:24]; 
				end
				4'b1001:
				begin 
					ALUResult_tmp[7:0] = (srcA[7:0] + srcB[7:0] > 255) ? 255 : srcA[7:0] + srcB[7:0];
					ALUResult_tmp[15:8] = ( srcA[15:8] + srcB[15:8] > 255 ) ? 255 : srcA[15:8] + srcB[15:8];
					ALUResult_tmp[23:16] = ( srcA[23:16] + srcB[23:16] > 255 ) ? 255 : srcA[23:16] + srcB[23:16];
					ALUResult_tmp[31:24] = (srcA[31:24] + srcB[31:24] > 255) ? 255 : srcA[31:24] + srcB[31:24] ; 

				end

				default: ALUResult_tmp = srcA + srcB;
		endcase

		zero_tmp <= (( ALUResult_tmp == 0) ? ( 1 ) : 0);
	end

endmodule
