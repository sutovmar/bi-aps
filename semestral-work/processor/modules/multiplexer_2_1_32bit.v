module multiplexer_2_1_32bit(input [31:0] d0, d1, input select, output [31:0] y );
	
	assign y = (select == 1)?d0:d1;

endmodule