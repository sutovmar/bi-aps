module multiplexer_4_1(input [31:0] d0, d1, d2,d3, input [2:0] select, output [31:0] y );
	
	reg [31:0] y_tmp;

	assign y = y_tmp;

	always @( d0 or d1 or d2 or d3 or select)
		case ( select )
			0: y_tmp <= d0;
			1: y_tmp <= d1;
			2: y_tmp <= d2;
			4: y_tmp <= d3;
			default: y_tmp <= 32'b0;
		endcase
endmodule