#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#define CENTRE 5
#define STRAIGHT -1

struct Histogram{

    int int1;
    int int2;
    int int3;
    int int4;
    int int5;
};

int main(int argc, char ** argv )
{
    // struct timespec start, stop;
    // clock_gettime( CLOCK_REALTIME, &start);
    
    int width, height,max,size,i;
    int red,green,blue,x,y,Y;
    struct Histogram Hist;
	
    Hist.int1 = 0;
    Hist.int2 = 0;
    Hist.int3 = 0;
    Hist.int4 = 0;
    Hist.int5 = 0;
    
    if ( argc != 2 )
		return 0;
	
	FILE * source_file = fopen(argv[1], "rb");

	fseek(source_file, 3, SEEK_SET);

    if ( !fscanf(source_file,"%d %d\n%d\n", &width, &height, &max))
        return 0;

    size = width * height * 3;

    unsigned char ** source_array = (unsigned  char**)(malloc( size ));

    for ( i = 0; i < height; i++) {
        
        source_array[i] = (unsigned char *) malloc(sizeof (unsigned char) * width * 3);
        
        if (!fread(source_array[i], sizeof (unsigned char), width * 3, source_file))
            return 1;
    }

    FILE * output_file = fopen("output.ppm","wb");
    FILE * histogram_f = fopen("output.txt","w");

    fprintf(output_file,"P6\n%d\n%d\n%d\n",  width, height, max);


    for(  y = 0; y < height; y++)
    {
    for(  x = 0; x < width*3; x+=3)
    {   	

    	if( y == 0 || y == height-1 || x == 0 || x == (width-1)*3 )
            {
                
                red  = source_array[y][x];
                green = source_array[y][x+1];
                blue = source_array[y][x+2];

            } else {


                red =    source_array[y][x]*(CENTRE) +
                          source_array[y][x-3]*(STRAIGHT) +
                          source_array[y][x+3]*(STRAIGHT) +
                          source_array[y+1][x]*(STRAIGHT) +
                          source_array[y-1][x]*(STRAIGHT) ;
                
                if( red > max)
                {
                    red = max;
                }

                if ( red < 0)
                {    
                    red = 0;
                }
                  green = source_array[y][x+1]*(CENTRE) +
                          source_array[y][x-3 + 1]*(STRAIGHT) +
                          source_array[y][x+3 + 1]*(STRAIGHT) +
                          source_array[y+1][x+1]*(STRAIGHT) +
                          source_array[y-1][x+1]*(STRAIGHT) ;

                if( green > max)
                {
                    green = max;
                }

                if ( green < 0)
                {
                    green = 0;
                }

                  blue = source_array[y][x+2]*(CENTRE) +
                          source_array[y][x+3 + 2]*(STRAIGHT)+
                          source_array[y][x-3 + 2]*(STRAIGHT) +
                          source_array[y+1][x+2]*(STRAIGHT) +
                          source_array[y-1][x+2]*(STRAIGHT);

                if( blue > max)
                {
                    blue = max;
                }

                if ( blue < 0)
                {
                    blue = 0;
                }
            }

            fprintf(output_file,"%c%c%c", (unsigned char)red, (unsigned char)green, (unsigned char)blue);
            
            Y = round( 0.2126*( red) + 0.7152*( green) + 0.0722*( blue));

            if ( Y <= 50 )
            {
            	Hist.int1++;

            } else if (  Y <= 101 ) 
            {
            	Hist.int2++;

            } else if ( Y <= 152 )
            {
            	Hist.int3++;

            } else if(  Y <= 203 ) {

            	Hist.int4++;

            } else {
            		
            	Hist.int5++;
            }
    	}
    }
 
    for ( i = 0; i < height; i++)
        free(source_array[i]);

    fprintf(histogram_f,"%d %d %d %d %d", Hist.int1,Hist.int2,Hist.int3,Hist.int4, Hist.int5);

    free(source_array);

   	fclose(source_file);
   	fclose(output_file);
    fclose(histogram_f);

    // clock_gettime( CLOCK_REALTIME, &stop);
    // double accum = ( stop.tv_sec - start.tv_sec )*1000.0 + ( stop.tv_nsec - start.tv_nsec )/ 1000000.0;
    // printf( "Time: %.6lf ms\n", accum );
   	
    return 1;
}
