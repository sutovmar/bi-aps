#!/bin/bash
test_ppm="e2ab0a3ed57eb3473015dc0cbe49fa70"
test_txt="23e7b856cd6d345e6c12c4c2c07f8256"
vit_small="32554ccd9b09af5b660a17b05350959b"
./main /root/Desktop/bi-aps/semestral-work/cache/test-10x8/test.ppm
md5_ppm=`md5sum output.ppm | cut -d " " -f1`
md5_txt=`md5sum output.txt  | cut -d " " -f1`


if [ $md5_ppm != $test_ppm ]; 
then
	echo "Output.ppm and test-10x8/output.ppm does not match"
	exit 0;
fi

if [ $md5_txt != $test_txt ];
then
	echo "Txt problem"
	exit 0
fi

./main /root/Desktop/bi-aps/semestral-work/cache/vit_small.ppm

md5_small_ppm=`md5sum output.ppm | cut -d " " -f1`

if [ $md5_small_ppm != $vit_small ];
then
	echo "Output.ppm and vit_small.ppm md5sum does not match"
	exit 0;
fi

echo "OK"
exit 1
